*** Variables ***

${CSVLOJAS}    ${EXECDIR}\\features\\data\\ListaLojas.csv
${XML_FILE}    ${EXECDIR}\\features\\POS\\xmlTemp\\ScreenLayout.xml
${CFG_FILE}    ${EXECDIR}\\features\\POS\\xmlTemp\\loader.cfg

*** Keywords ***
Validar as configurações de PDV nas Lojas BK
    ${BTN_PROBLEM_COUNT} =    Set Variable            ${0}
    ${IP_PROBLEM_COUNT} =     Set Variable            ${0}
    Set test variable         ${BTN_PROBLEM_COUNT}
    Set test variable         ${IP_PROBLEM_COUNT}

    log to console    ${\n}
    log to console    --------------------------------------------------------------------
    Log to console    >>>>>>>>>>>>>>>>>> INICIANDO A VALIDAÇÃO DE LOJAS <<<<<<<<<<<<<<<<<<    
    log to console    --------------------------------------------------------------------
    log to console    ${\n}
    ${QTDE_LOJAS}     Pegar o tamanho do CSV

    FOR                 ${i}                                                                       IN RANGE          ${QTDE_LOJAS}
    Ler dados do CSV    ${i}
    ${SIZE}             get length                                                                 ${POS_LOJA}
    log to console      LOJA: ${POS_LOJA.IP} 
    Run Keyword         Validar Existência do Botao Conectcar                                      ${POS_LOJA.IP}
    IF                  ${SIZE}>${3} and ${CONNECTION_PASSED}
    Validar cada PDV    
    ELSE IF             ${SIZE}<${3}
    log to console      essa loja não possui PDVS
    END
    log to console      -----------------------------------------------------------------------
    END

    log to console    ${\n}
    log to console    **************************** STATUS *******************************
    IF                ${CONNECTION_PASSED}
    Log to console    Quantidade de lojas com problema no botão Conectar: ${BTN_PROBLEM_COUNT}
    Log to console    Quantidade de lojas com PDVs com problema na configuração do IP: ${IP_PROBLEM_COUNT}
    ELSE
    Log to console    Não foi possivel realizar as validações para a Loja ${POS_LOJA.IP}
    END
    log to console    ********************************************************************
    log to console    --------------------------------------------------------------------
    Log to console    >>>>>>>>>>>>>>>>>> VALIDAÇÃO DE LOJAS FINALIZADO. <<<<<<<<<<<<<<<<<<                    
    log to console    --------------------------------------------------------------------
    log to console    .

Validar cada PDV
    ${SIZE}    get length            ${POS_LOJA}
    ${SIZE}    set variable          ${SIZE-3}
    ${SIZE}    set variable          ${SIZE/2}
    ${SIZE}    convert to integer    ${SIZE}
    FOR        ${i}                  IN RANGE       ${SIZE}

    ${PDV_ATUAL}          set variable                   ${POS_LOJA.PDV_${i}}
    Set test variable     ${PDV_ATUAL}
    log to console        Validando PDV ${PDV_ATUAL} 
    Run Keyword unless    ${CONNECTION_FAILED}           Validar XML de configuração de IP do PDV    ${POS_LOJA.IP_PDV_${i}}    ${PDV_ATUAL}
    Run Keyword unless    ${IP_PASSED}                   increment IP_PROBLEM
    END

increment BTN_PROBLEM
    ${BTN_PROBLEM_COUNT}    set variable            ${BTN_PROBLEM_COUNT+1}
    set test variable       ${BTN_PROBLEM_COUNT}

increment IP_PROBLEM
    ${IP_PROBLEM_COUNT}    set variable           ${IP_PROBLEM_COUNT+1}
    set test variable      ${IP_PROBLEM_COUNT}

Validar Existência do Botao Conectcar
    [Arguments]             ${IP}
    ${CONNECTION_PASSED}    Run Keyword And Continue On Failure    Run Keyword and return Status               ConnectFTP
    set suite variable      ${CONNECTION_PASSED}
    Run Keyword IF          ${CONNECTION_PASSED}                   validações botao                            
    ...                     ELSE                                   Atualizar API Monitoria Erro comunicacao    ${IP}         

validações botao
    ${CONNECTION_FAILED}           set variable            ${False}
    set test variable              ${CONNECTION_FAILED}
    download XML
    Validar a presenca do botao
    DisconnectFTP

Atualizar API Monitoria Erro comunicacao
    [Arguments]             ${IP}
    ${BTN_PASSED}           set variable    ${False}
    ${IP_PASSED}            set variable    ${False}
    ${CONNECTION_FAILED}    set variable    ${True}

    set suite variable    ${BTN_PASSED}
    set suite variable    ${IP_PASSED}
    set test variable     ${CONNECTION_FAILED}

    Log to console    ATENÇÂO: Não foi possível estabelecer conexão com a Loja
    PostPDVStatus     ${POS_LOJA.IP}                                              2

Validar XML de configuração de IP do PDV
    [Arguments]             ${IP}                                  ${PDV}
    ${CONNECTION_PASSED}    Run Keyword And Continue On Failure    Run Keyword and return Status               ConnectFTP
    Run Keyword IF          ${CONNECTION_PASSED}                   validacoes ip                               ${IP}         ${PDV}
    ...                     ELSE                                   Atualizar API Monitoria Erro comunicacao    ${IP}         

validacoes ip
    [Arguments]                     ${IP}     ${PDV}
    download cfg configuracao IP    ${PDV}
    Validar Configuracao de ip      ${IP}
    DisconnectFTP

Validar a presenca do botao
    ${BTN_PASSED}                Button conectCar is Present
    set test Variable            ${BTN_PASSED}
    IF                           ${BTN_PASSED}                  
    PostPDVStatus                ${POS_LOJA.IP}                 0
    Delete XML
    Delete CFG
    ELSE
    PostPDVStatus                ${POS_LOJA.IP}                 1
    # Copiar XML para Validação
    increment BTN_PROBLEM
    END

Validar Configuracao de ip
    [Arguments]             ${IP}    
    Config IP is Present    ${IP}

    IF                      ${IP_PASSED}    
    PostPDVStatus           ${PDV_ATUAL}    0
    Delete XML
    Delete CFG
    ELSE
    PostPDVStatus           ${PDV_ATUAL}    3
    increment IP_PROBLEM
    END

Delete XML
    Remove File    ${XML_FILE}

Delete CFG
    Remove File    ${CFG_FILE}

Copiar XML para Validação
    ${NEW_XML_FILE}    set variable    ${EXECDIR}\\features\\POS\\XmlError\\${POS_LOJA.IP}_ScreenLayout.xml
    Move File          ${XML_FILE}     ${NEW_XML_FILE}