*** Settings ***
Library    Collections
Library    SSHLibrary
Library    OperatingSystem
Library    String
Library    DebugLibrary
Library    RequestsLibrary
Library    JSONLibrary
Library    CSVLib
Library    XML


# Keywords:
Resource    keywords/kws_pos.robot


# Configurações do FTP:
Resource    ../features/POS/ftp_settings.robot
Resource    ../features/validacao/valida_xml.robot
Resource    ../features/validacao/valida_cfg.robot

# Configurações da API:
Resource    ../features/api/ResponseAPI.robot

# Configurações do CSV:
Resource    ../features/POS/CSV/csv_reader.robot

# # Configurações do XML:
# Resource    ../features/POS/XML/xml_reader.robot

# Hooks
# Resource    hooks.robot

*** Variables ***
${TOKEN}=    cdf55038-b88d-40a1-b754-94a4fe468e61