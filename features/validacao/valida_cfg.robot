*** Settings ***
Resource    ../../resources/base.robot

*** Variables ***
${IP_CONFIG_FILE}    ${EXECDIR}/features/POS/xmlTemp/loader.cfg

*** Keywords ***

Config IP is Present
    [Arguments]    ${IP}

    ${CFG}      Parse XML           ${IP_CONFIG_FILE}	
    ${URL}=     Get Element text    ${CFG}                xpath=group[@name='ElectronicPayment']/group[@name='ElectronicTypes']/group[@name='ConectCar']/key[@name='URL']/string
    ${Auth}=    Get Element text    ${CFG}                xpath=group[@name='ElectronicPayment']/group[@name='ElectronicTypes']/group[@name='ConectCar']/key[@name='Auth']/string

    ${IP_OK}      Run keyword and return status    should be equal    ${URL}     ${IP}
    ${AUTH_OK}    Run keyword and return status    should be equal    ${Auth}    ${TOKEN}

    ${IP_PASSED}         run keyword if    ${IP_OK} and ${AUTH_OK}    set variable    ${True}
    ...                  ELSE              set variable               ${False}
    set test variable    ${IP_PASSED}      

    Run Keyword IF    ${IP_PASSED}    Log to console    SUCESSO: O arquivo de configuração do ${PDV_ATUAL} foi Validada com sucesso!
    ...               ELSE            Log to console    ERRO: O arquivo de configuração não estava correto para o ${PDV_ATUAL} ...

    Run Keyword unless    ${IP_OK}                   Setar IP           ${CFG}               ${IP}
    Run Keyword unless    ${AUTH_OK}                 Setar AUTH         ${CFG}               ${TOKEN}
    Run Keyword unless    ${IP_OK} and ${AUTH_OK}    Upload CFG File    ${IP_CONFIG_FILE}

Setar IP
    [Arguments]         ${CFG}    ${IP}
    Set Element text    ${CFG}    ${IP}                xpath=group[@name='ElectronicPayment']/group[@name='ElectronicTypes']/group[@name='ConectCar']/key[@name='URL']/string
    Save XML            ${CFG}    ${IP_CONFIG_FILE}

Setar AUTH
    [Arguments]         ${CFG}    ${TOKEN}
    Set Element text    ${CFG}    ${TOKEN}             xpath=group[@name='ElectronicPayment']/group[@name='ElectronicTypes']/group[@name='ConectCar']/key[@name='Auth']/string
    Save XML            ${CFG}    ${IP_CONFIG_FILE}

