*** Settings ***
Resource    ../../resources/base.robot

*** Variables ***
${XML_BTN_CONECTCAR}    ${EXECDIR}/features/POS/xmlTemp/ScreenLayout.xml
${XML_BACKUP}           ${EXECDIR}/features/POS/XML_Backup/ScreenLayout.xml
${BUTTON}               <Button bgColor="#ed7801" bgColorPressed="default" border="none" borderPressed="none" customClass="rounded-corners" fontSize="17px" id="2200" position="356,375" size="310x65" text="ConectCar" textColor="white" textColorPressed="default" visible="false">

*** Keywords ***

Button conectCar is Present
    ${XML}             OperatingSystem.Get File                                        ${XML_BTN_CONECTCAR}    encoding=UTF-8
    ${STATUS}          run keyword and return status                                   Should Contain          ${XML}            ${BUTTON}
    IF                 ${STATUS}                                                       
    Log to console     SUCESSO: O botão está configurado na Loja IP: ${POS_LOJA.IP}
    ELSE
    Upload XML File    ${XML_BACKUP}
    END

    Run Keyword And Continue On Failure    Should Contain    ${XML}    ${BUTTON}    O Botão Conectar não estava configurado na Loja IP: ${POS_LOJA.IP} ...    values=False

    [Return]    ${STATUS}
