*** Settings ***
Resource    ../../resources/base.robot

*** Variables ***
${URL}=      http://23.96.50.109:5000
${TOKEN}=    cdf55038-b88d-40a1-b754-94a4fe468e61

*** Keywords ***
setupApiResponse
    Create Session    APIMonitoria    ${URL}    verify=true

PostPDVStatus
    [Arguments]     ${PDV}              ${STATUS}
    IF              '${STATUS}'=='0'
    ${DESCRICAO}    set variable        Sucesso
    ELSE IF         '${STATUS}'=='1'
    ${DESCRICAO}    set variable        Botão não disponível na interface
    ELSE IF         '${STATUS}'=='2'
    ${DESCRICAO}    set variable        Erro de comunicação em operações
    ELSE IF         '${STATUS}'=='3'
    ${DESCRICAO}    set variable        Arquivo de configurações indisponíveis
    END

    ${STATUS}    convert to integer    ${STATUS}
    setupApiResponse

    # configuração do cabeçalho e body da requisição
    &{headers}=    Create Dictionary         content-type=application/json    Authorization=${TOKEN}
    &{payload}=    Create Dictionary         
    ...            idPdv=${PDV}
    ...            status=${STATUS}
    ...            descricao=${DESCRICAO}

    ${RESP}=            Post Request                                APIMonitoria    /api/monitoria    data=${payload}    headers=${headers}
    Status Should Be    200                                         ${RESP}
    # log to console      ${RESP.text}
    log to console      Api de monitoria Atualizada com sucesso.