*** Settings ***
Resource    ../../resources/base.robot

*** Variables ***
${DIR_XML_BTN_CONECTCAR}    /home/administrador/mwpos_server/genesis/data/server/htdocs/pos


*** Keywords ***

ConnectFTP
    Open Connection    ${POS_LOJA.IP}
    Login              ${POS_LOJA.USER}    ${POS_LOJA.PASS}

download XML
    SSHLibrary.Directory Should Exist    ${DIR_XML_BTN_CONECTCAR}
    SSHLibrary.File Should Exist         ${DIR_XML_BTN_CONECTCAR}/ScreenLayout.xml
    SSHLibrary.get file                  ${DIR_XML_BTN_CONECTCAR}/ScreenLayout.xml    ${CURDIR}\\xmlTemp\\

download cfg configuracao IP
    [Arguments]                          ${PDV}
    ${CFG_IP}                            set variable            mwpos_server/genesis/data/${PDV}/bundles/electronic_payment/
    set test variable                    ${CFG_IP}
    SSHLibrary.Directory Should Exist    ${CFG_IP}
    SSHLibrary.File Should Exist         ${CFG_IP}/loader.cfg
    SSHLibrary.get file                  ${CFG_IP}/loader.cfg    ${CURDIR}\\xmlTemp\\

Upload XML File
    [Arguments]       ${XML_BACKUP}
    log to console    Fazendo o upload do arquivo XML configurado...
    Put File          ${XML_BACKUP}                                     ${DIR_XML_BTN_CONECTCAR} 
    log to console    Upload OK.

Upload CFG File
    [Arguments]       ${IP_CONFIG_FILE}
    log to console    Fazendo o upload do arquivo CFG configurado...
    Put File          ${IP_CONFIG_FILE}                                 ${CFG_IP}
    log to console    Upload OK.

DisconnectFTP
    Close Connection	