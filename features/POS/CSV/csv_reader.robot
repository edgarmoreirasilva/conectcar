*** Settings ***
Resource    ../../../resources/base.robot

*** Keywords ***

Pegar o tamanho do CSV
    ${CSV_FILE}=    Read CSV As list    ${CSVLOJAS}    ;
    ${SIZE}         get length          ${CSV_FILE}
    [Return]        ${SIZE}

Ler dados do CSV
    [Arguments]          ${ID}
    ${POS_LOJA}=         Create Dictionary
    ${LOJA}=             Read CSV As list      ${CSVLOJAS}               ;
    ${QTDE_PDVS}         get length            ${LOJA[${ID}]}
    ${QTDE_DADOS}        get length            ${LOJA[${ID}]}
    ${QTDE_PDVS}         set variable          ${QTDE_PDVS-3}
    ${QTDE_PDVS}         set variable          ${QTDE_PDVS/2}
    ${QTDE_PDVS}         convert to integer    ${QTDE_PDVS}
    set to Dictionary    ${POS_LOJA}           IP=${LOJA[${ID}][0]}
    set to Dictionary    ${POS_LOJA}           USER=${LOJA[${ID}][1]}
    set to Dictionary    ${POS_LOJA}           PASS=${LOJA[${ID}][2]}

    Run Keyword if       ${QTDE_PDVS}>${0}    Setar Pdvs    ${LOJA[${ID}]}    ${QTDE_DADOS}    ${POS_LOJA}
    Set test Variable    ${POS_LOJA}

Setar Pdvs
    [Arguments]          ${LOJA}               ${QTDE_PDVS}                         ${POS_LOJA}
    ${QTDE_PDVS}         set variable          ${QTDE_PDVS-3}
    ${QTDE_PDVS}         set variable          ${QTDE_PDVS/2}
    ${QTDE_PDVS}         convert to integer    ${QTDE_PDVS}
    ${x}                 set Variable          ${2}
    FOR                  ${i}                  IN RANGE                             ${QTDE_PDVS}
    ${PDV_INDEX}         set variable          ${x+1}
    ${IP_INDEX}          set variable          ${x+2}
    set to Dictionary    ${POS_LOJA}           PDV_${i}=${LOJA[${PDV_INDEX}]} 
    set to Dictionary    ${POS_LOJA}           IP_PDV_${i}=${LOJA[${IP_INDEX}]} 
    ${x}                 set variable          ${IP_INDEX}

    END

